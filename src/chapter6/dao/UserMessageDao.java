package chapter6.dao;
//DBの表結合から値を取得するためのDAO。
//データアクセスに関するコード(JDBCを利用したコード)

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserMessage;
import chapter6.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> select(Connection connection, Integer id, int num, String start, String end) {

    	//Prepared=用意された
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();

            //MYSOL ﾊﾞｲﾝﾄﾞ変数に値を設定
            sql.append("SELECT ");
            sql.append("    messages.id as id, ");
            sql.append("    messages.text as text, ");
            sql.append("    messages.user_id as user_id, ");
            sql.append("    users.account as account, ");
            sql.append("    users.name as name, ");
            sql.append("    messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("WHERE messages.created_date BETWEEN ? AND ? ");

            //条件を追加 WHEREの後は何の条件 ﾒｯｾｰｼﾞIDが一致しているか。
            if (id != null) {
            	sql.append(" AND messages.user_id = ? ");
            }
            //ORDER BY = ｿｰﾄ順指定 / DESC = 降順
            sql.append(" ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, start);
            ps.setString(2, end);

            if (id != null) {
            	ps.setInt(3, id);
            }

            //実行結果
            ResultSet rs = ps.executeQuery();

            List<UserMessage> messages = toUserMessages(rs);
            return messages;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {

        List<UserMessage> messages = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
                UserMessage message = new UserMessage();

                //select結果を参照する
                message.setId(rs.getInt("id"));
                message.setText(rs.getString("text"));
                message.setUserId(rs.getInt("user_id"));
                message.setAccount(rs.getString("account"));
                message.setName(rs.getString("name"));
                message.setCreatedDate(rs.getTimestamp("created_date"));

                messages.add(message);
            }
            return messages;
        } finally {
            close(rs);
        }
    }
}