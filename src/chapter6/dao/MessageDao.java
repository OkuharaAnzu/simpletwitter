package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.Message;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

  //つぶやきの投稿用メソッド
  public void insert(Connection connection, Message message) {

	//java.sql.PreparedStatementインタフェース(SQLｲﾝｼﾞｪｸｼｮﾝ)
	//プリコンパイルされたSQL文を表すオブジェクト
    PreparedStatement ps = null;
    try {
    	//StringBuilderクラスは文字列操作を行うためのクラス
      StringBuilder sql = new StringBuilder();

      		//文字列の結合
            sql.append("INSERT INTO messages ( ");
            sql.append("    user_id, ");
            sql.append("    text, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");                                 // user_id
            sql.append("    ?, ");                                  // text
            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
            sql.append("    CURRENT_TIMESTAMP ");       // updated_date
            sql.append(")");

      //
      ps = connection.prepareStatement(sql.toString());

      ps.setInt(1, message.getUserId());
      ps.setString(2, message.getText());

      ps.executeUpdate();
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
  }

  	//つぶやきの編集のためのメソッド
  	public Message select(Connection connection, String messageId) {

  		PreparedStatement ps = null;

  		try {
  			//idを設定 DBへ指定した内容を取得
  	  		String sql = "SELECT * FROM messages WHERE id = ?";

  	  		////psにSQL文を準備。実行前準備。
  	  		ps = connection.prepareStatement(sql);

  	  		//？に値をセット。
  	  		ps.setString(1, messageId);

  	  		//SQLを実行。select文の必須構文。
  	  		ResultSet rs = ps.executeQuery();

  	  		//addの結果がmessasesに入る
  	  		List<Message> messases = toMessages(rs);

  	  		if(messases.isEmpty()) {
  	  			return null;
  	  		}
  	  		return messases.get(0);

  	    } catch (SQLException e) {
  	        throw new SQLRuntimeException(e);
  	    } finally {
  	        close(ps);
  	    }
  	}

  	private List<Message> toMessages(ResultSet rs) throws SQLException {

    	//Userという箱をリスト型にしている
        List<Message> messages = new ArrayList<Message>();
        try {
        	//ループ処理
            while (rs.next()) {

                Message message = new Message();

                //実際に取ってきたデータをﾕｰｻﾞｰにｾｯﾄ。DBのテーブルからカラムを取得。
                message.setId(rs.getInt("id"));
                message.setText(rs.getString("text"));
                message.setCreatedDate(rs.getTimestamp("created_date"));
                message.setUpdatedDate(rs.getTimestamp("updated_date"));

                //データベースからセレクトしてきた結果
                messages.add(message);
            }
            //呼び出し元に返す。
            return messages;
        } finally {
            close(rs);
        }
    }

  	//つぶやきの更新のためのメソッド updateメソッド。
  	public void update(Connection connection, Message message) {

		 PreparedStatement ps = null;
	        try {
	        	//StringBuilderクラス=文字列の連結
	            StringBuilder sql = new StringBuilder();

	            //sql.append=SQLに文字列を追加する。更新したいカラムを記述。
	            sql.append("UPDATE messages SET ");
	            sql.append("    text = ?, ");
	            sql.append("    updated_date = CURRENT_TIMESTAMP ");
	            sql.append("WHERE id = ?");

	            //パラメータ付き SQL 文をデータベースに送るための PreparedStatement オブジェクトを生成
	            ps = connection.prepareStatement(sql.toString());

	            //データベースに送るときに、ドライバはこれをSQLに変換する。
	            ps.setString(1, message.getText());
	            ps.setInt(2, message.getId());

	            int count = ps.executeUpdate();
	            if (count == 0) {

	            	//例外ｵﾌﾞｼﾞｪｸﾄを生成して投げる
	                throw new NoRowsUpdatedRuntimeException();
	            }
	          //例外が投げられたらｷｬｯﾁし内容を出力。(引数eに渡される)
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        //最後に必ず行う処理
	        } finally {
	            close(ps);
	        }
	    }


  	//つぶやき削除用のメソッド
  	public void delete(Connection connection, Message message) {

		 PreparedStatement ps = null;
	        try {
	        	//StringBuilderクラス=文字列の連結
	            StringBuilder sql = new StringBuilder();

	            //sql.append=SQLに文字列を追加する。削除したいカラムを記述。
	            sql.append("DELETE FROM messages WHERE id = ?");

	            ps = connection.prepareStatement(sql.toString());

	            ps.setInt(1, message.getId());

	            int count = ps.executeUpdate();
	            if (count == 0) {

	            //例外ｵﾌﾞｼﾞｪｸﾄを生成して投げる
	            throw new NoRowsUpdatedRuntimeException();
	            }
	          //例外が投げられたらｷｬｯﾁし内容を出力。(引数eに渡される)
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        //最後に必ず行う処理
	        } finally {
	            close(ps);
	        }
	    }
}