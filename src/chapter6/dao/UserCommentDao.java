package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.UserComment;
import chapter6.exception.SQLRuntimeException;

public class UserCommentDao {

		//つぶやき返信コメント一覧を表示するメソッド
		public List<UserComment> select(Connection connection) {

	    	//Prepared=用意された
	        PreparedStatement ps = null;
	        try {
	            StringBuilder sql = new StringBuilder();

	            //MYSOL ﾊﾞｲﾝﾄﾞ変数に値を設定
	            sql.append("SELECT ");
	            //as = ｶﾗﾑに別名を付ける
	            sql.append("    comments.id as id, ");
	            sql.append("    comments.text as text, ");
	            sql.append("    comments.user_id as user_id, ");
	            sql.append("    comments.message_id as message_id, ");
	            sql.append("    users.account as account, ");
	            sql.append("    users.name as name, ");
	            sql.append("    comments.created_date as created_date ");
	            //表結合
	            sql.append("FROM comments ");
	            sql.append("INNER JOIN users ");
	            sql.append("ON comments.user_id = users.id ");
	            sql.append("INNER JOIN messages ");
	            sql.append("ON comments.message_id = messages.id");

//	            //投稿日時の降順で表示
//	            sql.append("ORDER BY created_date DESC limit ");

	            ps = connection.prepareStatement(sql.toString());

	            //実行結果
	            ResultSet rs = ps.executeQuery();

	            List<UserComment> comments = toUserComments(rs);
	            return comments;
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps);
	        }
	    }

		//
	    private List<UserComment> toUserComments(ResultSet rs) throws SQLException {

	        List<UserComment> comments = new ArrayList<UserComment>();
	        try {
	            while (rs.next()) {
	                UserComment comment = new UserComment();

	                //select結果を参照する 値を詰めている
	                comment.setId(rs.getInt("id"));
	                comment.setText(rs.getString("text"));
	                comment.setUserId(rs.getInt("user_id"));
	                comment.setAccount(rs.getString("account"));
	                comment.setName(rs.getString("name"));
	                comment.setMessageId(rs.getInt("message_id"));
	                comment.setCreatedDate(rs.getTimestamp("created_date"));

	                comments.add(comment);
	            }
	            return comments;
	        } finally {
	            close(rs);
	        }
	    }

}
