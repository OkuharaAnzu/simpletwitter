package chapter6.dao;
//JDBCを使用してDBからデータを追加するDaoｸﾗｽ。

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class UserDao {

	//データの追加
    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("    account, ");
            sql.append("    name, ");
            sql.append("    email, ");
            sql.append("    password, ");
            sql.append("    description, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");                                  // account
            sql.append("    ?, ");                                  // name
            sql.append("    ?, ");                                  // email
            sql.append("    ?, ");                                  // password
            sql.append("    ?, ");                                  // description
            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
            sql.append("    CURRENT_TIMESTAMP ");       // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getName());
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getPassword());
            ps.setString(5, user.getDescription());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User select(Connection connection, String accountOrEmail, String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE (account = ? OR email = ?) AND password = ?";

            ps = connection.prepareStatement(sql);

            ps.setString(1, accountOrEmail);
            ps.setString(2, accountOrEmail);
            ps.setString(3, password);

            //
            ResultSet rs = ps.executeQuery();

            //addの結果がusersに入る
            List<User> users = toUsers(rs);

            //usersが空だったら、nullが返る。
            if (users.isEmpty()) {
                return null;
            } else if (2 <= users.size()) {
                throw new IllegalStateException("ユーザーが重複しています");
            } else {
            	//serviceにﾘﾀｰﾝ。呼び出し元に返る。
                return users.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    //DBから取得してきたデータを
    private List<User> toUsers(ResultSet rs) throws SQLException {

    	//Userという箱をリスト型にしている
        List<User> users = new ArrayList<User>();
        try {
        	//ループ処理
            while (rs.next()) {

                User user = new User();

                //実際に取ってきたデータをﾕｰｻﾞｰにｾｯﾄ。
                user.setId(rs.getInt("id"));
                user.setAccount(rs.getString("account"));
                user.setName(rs.getString("name"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("password"));
                user.setDescription(rs.getString("description"));
                user.setCreatedDate(rs.getTimestamp("created_date"));
                user.setUpdatedDate(rs.getTimestamp("updated_date"));

                //データベースからセレクトしてきた結果
                users.add(user);
            }
            //
            return users;
        } finally {
            close(rs);
        }
    }
    //ﾕｰｻﾞｰの重複を確認するメソッド
    public User select(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
        	//idを設定。
            String sql = "SELECT * FROM users WHERE id = ?";

            //psにSQL文を準備。実行前準備。
            ps = connection.prepareStatement(sql);

            //？に値をセット。
            ps.setInt(1, id);

            //SQLを実行。rsに実行結果が入る。
            ResultSet rs = ps.executeQuery();

            //
            List<User> users = toUsers(rs);
            if (users.isEmpty()) {
                return null;
            } else if (2 <= users.size()) {
                throw new IllegalStateException("ユーザーが重複しています");
            } else {
                return users.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //ﾕｰｻﾞ-情報更新のメソッド
    public void update(Connection connection, User user) {

    	//DBに対するSQL文を実行
        PreparedStatement ps = null;
        try {
        	//StringBuilderクラス=文字列の連結
            StringBuilder sql = new StringBuilder();
            String password = user.getPassword();

            //sql.append=SQLに文字列を追加する。
            sql.append("UPDATE users SET ");
            sql.append("    account = ?, ");
            sql.append("    name = ?, ");
            sql.append("    email = ?, ");

            if (!StringUtils.isBlank(password)) {
            	sql.append("    password = ?, ");
            }
            sql.append("    description = ?, ");
            sql.append("    updated_date = CURRENT_TIMESTAMP ");
            sql.append("WHERE id = ?");

            //パラメータ付き SQL 文をデータベースに送るための PreparedStatement オブジェクトを生成
            ps = connection.prepareStatement(sql.toString());

            //データベースに送るときに、ドライバはこれをSQLに変換する。
            ps.setString(1, user.getAccount());
            ps.setString(2, user.getName());
            ps.setString(3, user.getEmail());

            if (!StringUtils.isBlank(password)) {
            	ps.setString(4, user.getPassword());
            	ps.setString(5, user.getDescription());
            	ps.setInt(6, user.getId());
            } else {
            	ps.setString(4, user.getDescription());
                ps.setInt(5, user.getId());
            }

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    /*
     * String型のaccountを引数にもつ、selectメソッドを追加する
     */
    public User select(Connection connection, String account) {

      PreparedStatement ps = null;
      try {
        String sql = "SELECT * FROM users WHERE account = ?";

        ps = connection.prepareStatement(sql);
        ps.setString(1, account);

        ResultSet rs = ps.executeQuery();

        List<User> users = toUsers(rs);
        if (users.isEmpty()) {
          return null;
        } else if (2 <= users.size()) {
          throw new IllegalStateException("ユーザーが重複しています");
        } else {
          return users.get(0);
        }
      } catch (SQLException e) {
        throw new SQLRuntimeException(e);
      } finally {
        close(ps);
      }
    }
}
