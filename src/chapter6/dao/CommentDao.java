package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import chapter6.beans.Comment;
import chapter6.exception.SQLRuntimeException;

public class CommentDao {

	//返信コメント投稿用のメソッド
	public void insert(Connection connection, Comment comment) {

		//java.sql.PreparedStatementインタフェース(SQLｲﾝｼﾞｪｸｼｮﾝ)
		//プリコンパイルされたSQL文を表すオブジェクト
	    PreparedStatement ps = null;
	    try {
	    	//StringBuilderクラスは文字列操作を行うためのクラス
	      StringBuilder sql = new StringBuilder();

	      		//INSERT＝ﾃｰﾌﾞﾙ(comments tabels)へﾃﾞｰﾀを登録する
	            sql.append("INSERT INTO comments ( ");
	            sql.append("    text, ");
	            sql.append("    user_id, ");
	            sql.append("    message_id,");
	            sql.append("    created_date, ");
	            sql.append("    updated_date ");
	            sql.append(") VALUES ( ");
	            sql.append("    ?, "); // text
	            sql.append("    ?, "); //user_id
	            sql.append("    ?, "); //message_id
	            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
	            sql.append("    CURRENT_TIMESTAMP ");       // updated_date
	            sql.append(")");

	      //
	      ps = connection.prepareStatement(sql.toString());

	      ps.setString(1, comment.getText());
	      ps.setInt(2, comment.getUserId());
	      ps.setInt(3, comment.getMessageId());

	      //DBへ返信コメント登録
	      ps.executeUpdate();
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	  }
}
