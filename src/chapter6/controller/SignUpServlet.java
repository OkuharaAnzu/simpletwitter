package chapter6.controller;
//ユーザー登録画面のServlet

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {

    @Override //getの場合signup.jspを表示
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//処理の転送
        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override //Post場合、Serviceﾒｿｯﾄﾞを呼び出してDBへﾕｰｻﾞｰ登録する。
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//ｴﾗｰﾒｯｾｰｼﾞﾘｽﾄを作成
        List<String> errorMessages = new ArrayList<String>();

        //
        User user = getUser(request);
        if (!isValid(user, errorMessages)) {
            request.setAttribute("errorMessages", errorMessages);

            //ユーザー登録が完了したら、TOPJSPを表示
            request.getRequestDispatcher("signup.jsp").forward(request, response);
            return;
        }

        new UserService().insert(user);
        //ﾘﾀﾞｲﾚｸﾄして画面表示。フォームデータの2重送信防止(PRGﾊﾟﾀｰﾝ)。
        response.sendRedirect("./");
    }

    private User getUser(HttpServletRequest request) throws IOException, ServletException {

        User user = new User();
        user.setName(request.getParameter("name"));
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setEmail(request.getParameter("email"));
        user.setDescription(request.getParameter("description"));
        return user;
    }

    private boolean isValid(User user, List<String> errorMessages) {

    String name = user.getName();
        String account = user.getAccount();
        String password = user.getPassword();
        String email = user.getEmail();


        //ﾕｰザｰ型の情報が返ってくる
        new UserService().select(account);
        //返ってきた値を代入
        User checkUser =  new UserService().select(account);

        if (!StringUtils.isEmpty(name) && (20 < name.length())) {
            errorMessages.add("名前は20文字以下で入力してください");
        }

        if (StringUtils.isEmpty(account)) {
            errorMessages.add("アカウント名を入力してください");
        } else if (20 < account.length()) {
            errorMessages.add("アカウント名は20文字以下で入力してください");
        } else if (checkUser != null) {
        	errorMessages.add("このアカウント名は既に使われています");

        }
        if (StringUtils.isEmpty(password)) {
            errorMessages.add("パスワードを入力してください");
        }

        if (!StringUtils.isEmpty(email) && (50 < email.length())) {
            errorMessages.add("メールアドレスは50文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}
