package chapter6.controller;
//TOP画面表示のサーブレット

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.beans.UserMessage;
import chapter6.service.CommentService;
import chapter6.service.MessageService;

//ｱﾉﾃｰｼｮﾝ /index.jspというURLと対応(ﾘｸｴｽﾄがあったら実行される)
@WebServlet(urlPatterns = { "/index.jsp" })

//HttpServletクラスを継承。ﾘｸｴｽﾄを受信し、ﾚｽﾎﾟﾝｽを返すための受け口。
public class TopServlet extends HttpServlet {

	//通常ｵｰﾊﾞｰﾗｲﾄﾞする
    @Override
    //ﾛｸﾞｲﾝのﾘｸｴｽﾄを受け取るメソッド。HttpServlet = ﾘｸｴｽﾄを受信し、ﾚｽﾎﾟﾝｽを返すための受け口
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//isShowMessageFormにfalseを代入
        boolean isShowMessageForm = false;

        //TOPJSPから入力されたloginUserの値をuserに代入。
        User user = (User) request.getSession().getAttribute("loginUser");

        //ﾛｸﾞｲﾝﾕｰｻﾞｰがnullでなかったら
        if (user != null) {
            isShowMessageForm = true;
        }

        //String型のuser_idの値をrequest.getParameter("user_id")でJSPから受け取るように設定
        String userId = request.getParameter("user_id");
        String start = request.getParameter("start");
        String end = request.getParameter("end");


        //MessageServiceのgetMessageに引数としてString型のuser_idを追加
        List<UserMessage> messages = new MessageService().select(userId, start, end);

        boolean isShowCommentForm = false;
        if (user != null) {
        	isShowCommentForm = true;
        }
        List<UserComment> comments = new CommentService().select();

        System.out.println(comments.get(0).getText());


        //ﾘｸｴｽﾄにmessagesというｷｰで値をセット ServletとJSPから参照できる
        request.setAttribute("messages", messages);
        request.setAttribute("isShowMessageForm", isShowMessageForm);

        //ﾘｸｴｽﾄにｺﾒﾝﾄをｾｯﾄ jsp側に渡す
		request.setAttribute("comments", comments);
        request.setAttribute("isShowCommentForm", isShowCommentForm);
        request.setAttribute("startDate", start);
        request.setAttribute("endDate", end);
        request.getRequestDispatcher("/top.jsp").forward(request, response);

    }
}