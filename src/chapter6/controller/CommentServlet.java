package chapter6.controller;
//コメントの返信のサーブレット

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Comment;
import chapter6.beans.User;
import chapter6.service.CommentService;

//ｱﾉﾃｰｼｮﾝを使ってURLのｻｰﾌﾞﾚｯﾄの呼び出し名をつける
@WebServlet(urlPatterns = {"/comment"})

//ｻｰﾊﾞのHTTP世界
public class CommentServlet extends HttpServlet{

	//doPostﾒｿｯﾄﾞで値を登録
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
		throws IOException, ServletException {

		HttpSession session = request.getSession();
		//エラーメッセージのリストを作っている
		List<String> errorMessages = new ArrayList<String>();

		//getParameter = Webﾌﾞﾗｳｻﾞから返信のｺﾒﾝﾄを受け取る
		String reply = request.getParameter("reply");



		//ﾘｸｴｽﾄからﾒｯｾｰｼﾞidを取得
		int messageId = Integer.parseInt(request.getParameter("messageId"));

		 if (!isValid(reply, errorMessages)) {
	         session.setAttribute("errorMessages", errorMessages);
	         response.sendRedirect("./");
	            return;
	     }


		 Comment comment = new Comment();
	     comment.setText(reply);
	     comment.setMessageId(messageId);

	     User user = (User) session.getAttribute("loginUser");
	     comment.setUserId(user.getId());

	     new CommentService().insert(comment);

	     response.sendRedirect("index.jsp");
	}

	private boolean isValid(String reply, List<String> errorMessages) {

        if (StringUtils.isBlank(reply)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < reply.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}
