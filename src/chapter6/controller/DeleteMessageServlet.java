package chapter6.controller;
//つぶやきを削除するためのServlet

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.Message;
import chapter6.service.MessageService;



@WebServlet(urlPatterns = { "/deleteMessage" })
public class DeleteMessageServlet extends HttpServlet {

	//requestは戻り値ではなく引数で受け取る。受け取ったｵﾌﾞｼﾞｪｸﾄをﾎﾞﾃﾞｨで組み立てる
	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

		//String型のerrorMessageというﾘｽﾄを生成。
        List<String> errorMessages = new ArrayList<String>();

      //ﾘｸｴｽﾄを取得。HttpServletRequestﾒｿｯﾄﾞでﾘｸｴｽﾄされたURLの特定の箇所を取得
	    String messageId = request.getParameter("messageId");
	    Message message = new MessageService().select(messageId);

	    //ﾊﾞﾘﾃﾞｰｼｮﾝ。ｾｯｼｮﾝに値をｾｯﾄする。textとerrorMessagesが無効の場合
	    if (!isValid(message, errorMessages)) {
	    	request.setAttribute("errorMessages", errorMessages);


	        //sessionにtextという名前を付けたつぶやきを格納。ﾘｸｴｽﾄの範囲で値を保持。
	        request.setAttribute("message", message);
	        request.getRequestDispatcher("edit.jsp").forward(request, response);
	        	return;
	    }

	    //ﾒｯｾｰｼﾞ削除
	    new MessageService().delete(message);

	    //ﾚｽﾎﾟﾝｽ情報をﾄｯﾌﾟ画面に返す
	    response.sendRedirect("./index.jsp");
	}

	 //オブジェクトが閉じられておらず、有効であるかどうかを示します
    private boolean isValid(Message message, List<String> errorMessages) {

    	//ﾒｯｾｰｼﾞIDが取得できなかったらｴﾗｰ表示
        if (message == null) {
            errorMessages.add("メッセージが見つかりません");
        }
        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}
