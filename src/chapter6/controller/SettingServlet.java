package chapter6.controller;
//ユーザー情報変更画面(設定)のServlet

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet {

    private HttpServletRequest request;

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    	//セッションを取得。システムの一連の流れ。処理の中での値を取ってくる。
        HttpSession session = request.getSession();
        //セッションから値を取得。ﾛｸﾞｲﾝﾕｰｻﾞｰ＝自分
        User loginUser = (User) session.getAttribute("loginUser");

        //userに自分の値を代入
        User user = new UserService().select(loginUser.getId());

        request.setAttribute("user", user);
        request.getRequestDispatcher("setting.jsp").forward(request, response);
    }

	//エラーメッセージの表示
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        List<String> errorMessages = new ArrayList<String>();

        User user = getUser(request);
        if (isValid(user, errorMessages)) {
            try {
                new UserService().update(user);
            } catch (NoRowsUpdatedRuntimeException e) {
                errorMessages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
            }
        }

        if (errorMessages.size() != 0) {
            request.setAttribute("errorMessages", errorMessages);
            request.setAttribute("user", user);
            request.getRequestDispatcher("setting.jsp").forward(request, response);
            return;
        }

        session.setAttribute("loginUser", user);
        response.sendRedirect("./");
    }

    //userインスタンスを生成。値をセットしuserに返す。
    private User getUser(HttpServletRequest request) throws IOException, ServletException {

        User user = new User();
        user.setId(Integer.parseInt(request.getParameter("id")));
        user.setName(request.getParameter("name"));
        user.setAccount(request.getParameter("account"));
        user.setPassword(request.getParameter("password"));
        user.setEmail(request.getParameter("email"));
        user.setDescription(request.getParameter("description"));
        return user;
    }

    //SQLが閉じられておらず、有効であるかどうかを示す。
    private boolean isValid(User user, List<String> errorMessages) {

        String name = user.getName();
        String account = user.getAccount();
        String email = user.getEmail();
        int id = user.getId();

        //ﾕｰザｰ型の情報が返ってくる。//返ってきた値を代入
        User checkUser = new UserService().select(account);


        //isEmpty→空文字判定
        if (!StringUtils.isEmpty(name) && (20 < name.length())) {
            errorMessages.add("名前は20文字以下で入力してください");
        }
        if (StringUtils.isEmpty(account)) {
            errorMessages.add("アカウント名を入力してください");
        } else if (20 < account.length()) {
            errorMessages.add("アカウント名は20文字以下で入力してください");
        } else if ((checkUser != null) && checkUser.getId() != id) {
        	errorMessages.add("このアカウント名は既に使われています");
        }
        if (!StringUtils.isEmpty(email) && (50 < email.length())) {
            errorMessages.add("メールアドレスは50文字以下で入力してください");
        }
        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}
