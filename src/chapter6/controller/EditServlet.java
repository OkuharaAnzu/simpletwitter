//つぶやきの編集のServlet
package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	//編集したいつぶやきidを選択。doGetメソッド…値を取ってくるとき
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//ﾊﾟﾗﾒｰﾀ(JSP)でﾘｸｴｽﾄされたﾒｯｾｰｼﾞIDを取得
    	String messageId = request.getParameter("messageId");

    	//DBの情報をmessageに代入
    	Message message = new MessageService().select(messageId);

    	HttpSession session = request.getSession();
    	List<String> errorMessages = new ArrayList<String>();

    	if(message == null) {
    		 errorMessages.add("不正なパラメータが入力されました");
    		 session.setAttribute("errorMessages", errorMessages);
    		 response.sendRedirect("./");
    		 return;
    	}

        //Editｻｰﾌﾞﾚｯﾄ処理後に表示するJSPに転送する
        request.setAttribute("message", message);
        request.getRequestDispatcher("edit.jsp").forward(request, response);
    }

    //doPostメソッド…DBに変更、修正、更新を加えるとき
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        //String型のerrorMessageというﾘｽﾄを生成。
        List<String> errorMessages = new ArrayList<String>();

        //入力されたつぶやきをﾊﾟﾗﾒｰﾀで受け取っている。
        String text = request.getParameter("text");

        //ﾊﾟﾗﾒｰﾀでﾘｸｴｽﾄされたﾒｯｾｰｼﾞIDを取得
        String messageId = request.getParameter("messageId");


        //ﾊﾞﾘﾃﾞｰｼｮﾝ。ｾｯｼｮﾝに値をｾｯﾄする。textとerrorMessagesが無効の場合
        if (!isValid(text, errorMessages)) {
        	Message message = new MessageService().select(messageId);

        	request.setAttribute("errorMessages", errorMessages);

            //sessionにtextという名前を付けたつぶやきを格納。ﾘｸｴｽﾄの範囲で値を保持。
        	request.setAttribute("message", message);
            request.getRequestDispatcher("edit.jsp").forward(request, response);

            return;
        }

        // messageにedit.jspで入力されたつぶやきを代入
        Message message = new Message();
        message.setText(text);
        message.setId(Integer.parseInt(messageId));

        //メッセージの更新
        new MessageService().update(message);

        //処理中のページから別のページ(ファイル)へ処理を移す
        response.sendRedirect("./");
    }

    //オブジェクトが閉じられておらず、有効であるかどうかを示します
    private boolean isValid(String text, List<String> errorMessages) {

    	//つぶやきの記入がなかったらｴﾗｰ表示
        if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        //
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}