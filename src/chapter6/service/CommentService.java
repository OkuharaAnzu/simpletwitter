package chapter6.service;
//返信コメントの処理をしているｻｰﾋﾞｽｸﾗｽ

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter6.beans.Comment;
import chapter6.beans.UserComment;
import chapter6.dao.CommentDao;
import chapter6.dao.UserCommentDao;

public class CommentService {

	//返信コメントをDBへ登録するﾒｿｯﾄﾞ
	public void insert(Comment comment) {

        Connection connection = null;
        try {
            connection = getConnection();
            new CommentDao().insert(connection, comment);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	//コメントの返信をDBから取得しcommentSercletへ値を返す。
	public List<UserComment> select() {

        Connection connection = null;
        try {
        	//DBへ接続
            connection = getConnection();

            //commentsにUserCommnetDaoのｾﾚｸﾄﾒｿｯﾄﾞを呼び出し(connection)を代入
            List<UserComment> comments = new UserCommentDao().select(connection);
            //DBへ保存
            commit(connection);

            //戻り値は connection
            return comments;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
