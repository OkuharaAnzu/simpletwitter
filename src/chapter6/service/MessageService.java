//つぶやきﾒｯｾｰｼﾞを登録するServiceクラス
//MessageBeanをMessageService#insert()の引数として渡して登録
package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService<DateTimeFormatter> {

	//つぶやきの登録
    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //ホーム画面へのメッセージ表示。Beans UserMessage.java から値を取得
    public List<UserMessage> select(String userId, String start, String end) {

    	//ホーム画面でつぶやきを1000件まで表示
        final int LIMIT_NUM = 1000;

        Connection connection = null;

        //Dateクラスのインスタンスを取得。その時点での日時を格納。
        Date dateObj = new Date();

        //SimpleDateFormatを使用したい日時表記の形式を指定してインスタンス化
        SimpleDateFormat format = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );

        //日時指定ﾌｫｰﾏｯﾄを文字列で指定
        String display = format.format( dateObj );
        String startDate = "2021-03-01 00:00:00";

        try {
        	//DBへ接続
            connection = getConnection();

            if (!StringUtils.isBlank(start)) {
            	//ｶﾚﾝﾀﾞｰ入力は日付までしか指定できない。時間を明示的に付ける。
            	start = start + " 00:00:00";
            } else {
            	start = startDate;
            }
            if (!StringUtils.isBlank(end)) {
            	end = end + " 23:59:59";
            } else {
            	end = display;
            }

            Integer id = null;

            //userIdが空文字でなかったら、idに代入
            if(!StringUtils.isEmpty(userId)) {
              id = Integer.parseInt(userId);
            }

            //massagesにUserMessageDaoのｯｾﾚｸﾄﾒｿｯﾄﾞを呼び出し(connection, id, LIMIT_NUM)を代入
            List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, start, end);
            //DBへ保存
            commit(connection);

            //戻り値は connection, id, LIMIT_NUM
            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    //つぶやきの編集 編集したい投稿をDBから取ってくる
    public Message select(String messageId) {

        Connection connection = null;
        try {
            connection = getConnection();
            Message message = new MessageDao().select(connection, messageId);
            commit(connection);

            //EditServletに値を返す
            return message;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

    }


    //つぶやき更新用のメソッド
    public void update(Message message) {

        Connection connection = null;

        try {
        	//connection=DBへつなげる
            connection = getConnection();
            new MessageDao().update(connection, message);
            commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    //つぶやき削除用メソッド
    public void delete(Message message) {

        Connection connection = null;

        try {
        	//connection=DBへつなげる
            connection = getConnection();
            new MessageDao().delete(connection, message);
            commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}