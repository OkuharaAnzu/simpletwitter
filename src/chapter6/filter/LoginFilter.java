package chapter6.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;

//どのURLに対してこのﾌｨﾙﾀｰを実行するのか /* = 全ﾍﾟｰｼﾞ
@WebFilter(urlPatterns = {"/edit", "/setting"})
public class LoginFilter implements Filter {

	//doFilter = メイン。
	@Override
	  public void doFilter(ServletRequest request, ServletResponse response,
	      FilterChain chain) throws IOException, ServletException {

			//引数のrequestをHttpServletRequest型に変換した変数の宣言
			HttpServletRequest req = (HttpServletRequest) request;
			HttpServletResponse res = (HttpServletResponse) response;

			HttpSession session = req.getSession();

			//ｾｯｼｮﾝ領域のloginUser
			User user = (User) req.getSession().getAttribute("loginUser");
			String servletPath = req.getServletPath();


			//ﾛｸﾞｲﾝしている状態であればそのまま表示
			if(servletPath.equals("/login") || user != null){
				chain.doFilter(request, response);

			} else {
				//上記以外はエラーメッセージとともに、ﾛｸﾞｲﾝ画面に遷移
				List<String> errorMessages = new ArrayList<String>();
				errorMessages.add("ログインしてください");
				session.setAttribute("errorMessages", errorMessages);
				res.sendRedirect("./login");
			}
	    }

		//init = ｱﾌﾟﾘｹｰｼｮﾝ起動時に必要なﾒｿｯﾄﾞ
	    @Override
		public void init(FilterConfig filterConfig) throws ServletException {
		}

	    //destroy = ｱﾌﾟﾘｹｰｼｮﾝ終了時に必要なﾒｿｯﾄﾞ(今回処理は無い。しかし必ず定義する)
	    @Override
	    public void destroy() {
	    }
}
