<%-- つぶやきの編集画面のJSP --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%-- ﾛｸﾞｲﾝ時エラーが起きたときエラー表示 --%>
	<c:if test="${ not empty errorMessages }">
    	<div class="errorMessages">
       		<ul>
        		<%-- Servlet上で errorMessages というｷｰに--%>
        		<%-- 繰り返しでｴﾗｰﾒｯｾｰｼﾞを表示 --%>
            	<c:forEach items="${errorMessages}" var="errorMessage">
                	<li><c:out value="${errorMessage}" />
            	</c:forEach>
        	</ul>
    	</div>
	</c:if>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>つぶやきの編集画面</title>
	</head>

	<body>
		<div class ="edit-area">
			<%-- editServletのpostメソッドへ送る --%>
			<form action="edit" method="post">
				編集内容<br />
				<%-- ${message.text}で編集前のつぶやきを表示 --%>
        		<textarea name="text" cols="100" rows="5" class="tweet-box">${message.text}</textarea>
        		<br />
        		<input name="messageId" value="${message.id}" id="id" type="hidden"/>
        		<input type="submit" value="更新">（140文字まで)
       			<br>
       			<%-- ./ = ﾃﾞﾌｫﾙﾄに設定(topjsp)に戻る --%>
        		<a href="./">戻る</a>
    		</form>
		</div>
	</body>
</html>