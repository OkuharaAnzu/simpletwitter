<%-- TOP画面のJSP --%>
<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    	<%-- --%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>簡易Twitter</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<script src = "https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js" ></script>
		<script>
			$(function() {
				$('#delete').on('click',function() {
        		alert('本当に削除しますか');
        		});
			});
		</script>
	</head>

    <body>
    	<%-- main-contentsとheaderというdivのまとまり--%>
        <div class="main-contents">
        	<div class="header">

        		<%-- test属性(必須)にはEL式で判定条件を指定 empty=空文字判定(nullかどうか)--%>
    			<c:if test="${ empty loginUser }">

        			<%-- a href=ｱﾝｶｰﾀﾞｸﾞのｴｲﾁﾚﾌ … ﾘﾝｸ先を指定--%>
        			<a href="login">ログイン</a>
       				<a href="signup">登録する</a>
    			</c:if>

    			<%-- ﾛｸﾞｲﾝしている状態 --%>
   				<c:if test="${ not empty loginUser }">
       				<a href="./">ホーム</a>
       				<a href="setting">設定</a>
       				<a href="logout">ログアウト</a>
       			</c:if>
       		</div>

		<%-- 日付絞り込み入力欄 --%>
		<div class="date-form">
				<form action="./" method="get">
					<%-- type属性でtype=dateを指定し日付の入力欄を作成 --%>
					日付 <input type="date" name="start" value = "${startDate}" min = "2021-03-01">
					～<input type="date" name="end" value = "${endDate}">
					<input type="submit" value="絞込">
				</form>
		</div>

		<%-- loginUserが空でなかったら出力 --%>
		<c:if test="${ not empty loginUser }">
		<%-- 人間が分かりやすくするためにｸﾗｽでまとめている --%>
    		<div class="profile">
    			<%-- c:out=出力ﾀｸﾞ --%>
        		<div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
        		<div class="account">@<c:out value="${loginUser.account}" /></div>
        		<div class="description"><c:out value="${loginUser.description}" /></div>
			</div>
		</c:if>

		<%-- ﾛｸﾞｲﾝ時エラーが起きたときエラー表示 --%>
    	<div class="errorMessages">
    		<c:if test="${ not empty errorMessages }">

        		<ul><%-- ul = 箇条書きのリスト --%>

        			<%-- Servlet上で errorMessages というｷｰに--%>
        			<%-- 繰り返しでｴﾗｰﾒｯｾｰｼﾞを表示 --%>
            		<c:forEach items="${errorMessages}" var="errorMessages">
                		<li><c:out value="${errorMessages}" />
            		</c:forEach>
        		</ul>
        		<%-- ｴﾗｰﾒｯｾｰｼﾞの削除 --%>
    			<c:remove var="errorMessages" scope="session" />
    		</c:if>
    	</div>


		<div class="form-area">

			<%-- TopServretから 条件指定--%>
    		<c:if test="${ isShowMessageForm }">

    			<%-- ﾒｯｾｰｼﾞ画面に遷移 form action="message"=ﾒｯｾｰｼﾞ(messageServlet)というﾍﾟｰｼﾞに対してPOST--%>
        		<%-- formﾀｸﾞ=DBにﾃﾞｰﾀを送るとき使う。どこのﾍﾟｰｼﾞをどうする。  method=方法--%>
        		<form action="message" method="post">
            		いま、どうしてる？<br />
            		<%-- textareaﾀｸﾞ cols=横幅 rows=縦幅 class=名前--%>
            		<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
            		<br />
            		<%-- inputﾀｸﾞ=formﾀｸﾞとｾｯﾄで使う。どうやって messageを通して処理 --%>
            		<%-- type=機能をつける submit=ﾒｯｾｰｼﾞを送る--%>
            		<input type="submit" value="つぶやく">（140文字まで）
        		</form>
    		</c:if>
		</div>

		<div class="messages">
			<%-- items=条件指定。ﾒｯｾｰｼﾞを引っ張て来ている--%>
    		<c:forEach items="${messages}" var="message">
        		<div class="message">
            		<div class="account-name">
                		<span class="account">

                			<%-- ｱｶｳﾝﾄ名を押してｱｶｳﾝﾄのつぶやき一覧表示。DBから。hlef=ﾘﾝｸ --%>
    						<a href="./?user_id=<c:out value="${message.userId}"/> ">
        						<c:out value="${message.account}" />
    						</a>
    					</span>
                		<span class="name"><c:out value="${message.name}" /></span>
            		</div>
            		<%-- ｱｶｳﾝﾄごとに表示 forEachで繰り返し --%>
            		<c:forEach var="messageNewLine" items="${fn:split(message.text, '
')}">
            			<div class="text"><c:out value = "${messageNewLine}" /></div>
            		</c:forEach>
 					<div class="date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
					<%-- ボタンの横並び指示 --%>
					<div style="display:inline-flex">

						<%-- つぶやき編集ボタン --%>
						<c:if test = "${loginUser.id == message.userId}">
							<form action="edit" method="get">
								<%-- ${message.id}送っている --%>
            					<input name="messageId" value="${message.id}" id="id" type="hidden"/>
            	 				<input type="submit" value="編集" >
            				</form>
            				<pre>  </pre><%-- スペース --%>
        				</c:if>
        				<c:if test = "${loginUser.id == message.userId}">
							<%-- つぶやきの削除ボタン --%>
							<form action="deleteMessage" method="post">
								<%-- ${message.id}送っている  hidden=ﾌﾞﾗｳｻﾞ上で非表示にしたいときに使う--%>
            					<input name="messageId" value="${message.id}" id="id" type="hidden"/>
            					<input type="submit" value="削除" id = "delete" >
            				</form>
        				</c:if>
        			</div>

					<%-- コメントの返信を表示 --%>
					<div class="comments">

    					<c:forEach items="${comments}" var="comment">
        					<div class="comment">
            					<div class="account-name">
            						<c:if test = "${comment.messageId == message.id}">

                						<span class="account"><c:out value="${comment.account}" /></span>
                						<span class="name"><c:out value="${comment.name}" /></span>

											<%-- ﾃｷｽﾄの改行 --%>
                							<c:forEach var="commentNewLine" items="${fn:split(comment.text, '
')}">
 												<div class="text"><c:out value = "${commentNewLine}" /></div>
            								</c:forEach>
            							<div class="date"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
        							</c:if>
        						</div>
        					</div>
   						</c:forEach>
					</div>

        			<div class="comment-area">
        				<c:if test="${ isShowCommentForm }">
							<%-- TopServretから 条件指定 ﾛｸﾞｲﾝしている状態なら見える--%>

    						<%-- ﾒｯｾｰｼﾞ画面に遷移 form action="comment"=ｺﾒﾝﾄ(commentServlet)というﾍﾟｰｼﾞに対してPOST--%>
        					<%-- formﾀｸﾞ=DBにﾃﾞｰﾀを送るとき使う。どこのﾍﾟｰｼﾞをどうする。  method=方法--%>
        					<form action="comment" method="post">
            					返信<br />

            					<%-- textareaﾀｸﾞ cols=横幅 rows=縦幅 class=名前--%>
            					<textarea name="reply" cols="100" rows="5" class="reply-box" ></textarea>


            					<br />
            					<%-- inputﾀｸﾞ=formﾀｸﾞとｾｯﾄで使う --%>
            					<input name="messageId" value="${message.id}" id="id" type="hidden"/>

            					<%-- type=機能をつける submit=ﾒｯｾｰｼﾞを送る--%>
            					<input type="submit" value="返信">
        					</form>
    					</c:if>
					</div>
    			</div>
    		</c:forEach>
		</div>
	</div>
    <div class="copyright"> Copyright(c)Anzu Okuhara</div>
    </body>
</html>